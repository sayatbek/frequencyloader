#Author

@Sayatbek 
ISTEP Solutions
Dortmund, Germany
2018

#Frequencyloader project

This project was designed to load or migrate the words with their frequency form a 
"12.pdf" file to a mysql database. 

#Clone the source code

Go to:

https://bitbucket.org/sayatbek/frequencyloader

or run this bash command:

```
$ git clone https://sayatbek@bitbucket.org/sayatbek/frequencyloader.git
```

#Running locally

Run the main method of this project as a simple Java Application.