import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Word {
  String name;
  String type;
  Integer quantity;
  Double percentage;

  @Override
  public String toString(){
    return name + " - " + type + " - " + quantity + " - " + percentage;
  }
}