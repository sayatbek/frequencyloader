import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

public class Main {

  public static void readWordsFromFile(List<Word> wordList) {
    try {
      PDDocument document = null;
      document = PDDocument.load(new File("12.pdf"));

      document.getClass();

      if (!document.isEncrypted()) {

        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
        stripper.setSortByPosition(true);

        PDFTextStripper tStripper = new PDFTextStripper();

        String pdfFileInText = tStripper.getText(document);
        String lines[] = pdfFileInText.split("\\r?\\n");
        for (String line : lines) {
          String[] arr = line.split(" ");
          if (arr.length > 3 && isNumeric(arr[0])) {
            Word word = new Word();
            try {
              word.setName(arr[1].split("/")[0]);
              word.setType(arr[1].split("/")[1]);
              word.setQuantity(Integer.parseInt(arr[2]));
              word.setPercentage(Double.parseDouble(arr[3].replace(",", ".")));
              // System.out.println(word);
              wordList.add(word);
            } catch (Exception e) {
              //System.out.println(e.getMessage());
            }
          }
        }
        document.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static Connection getPostGresConnection() {
    Connection connection = null;
    try {
      Class.forName("org.postgresql.Driver");
      connection = DriverManager.getConnection(
          "jdbc:postgresql://izdepkz.chufvdctrheb.us-west-2.rds.amazonaws.com:5432/izdepkz", "postgres", "aqw123355");
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    return connection;
  }

  public static void executeUpdate(Connection connection, String query){
    Statement stmt = null;
    try {
      stmt = connection.createStatement();
      stmt.executeUpdate(query);
      stmt.close();
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
  }

  public static void createTableIfNotExist(Connection connection) {
    String query = "create table if not exists public.terms (id integer, name varchar(255), "
        + "type varchar(255), quantity integer, percentage numeric)";
    executeUpdate(connection, query);
  }

  public static void insertTermsInTable(Connection connection, List<Word> wordList){
    String query = "insert into public.terms(id, name, type, quantity, percentage) "
        + "values ";
    Iterator<Word> it = wordList.listIterator();
    int i = 0, j = 0;
    while (it.hasNext()) {
      Word w = it.next();
      if (it.hasNext()) {
        query += "("+i+", '" + w.getName() + "', '" + w.getType() + "', " + w.getQuantity() + ", " + w
            .getPercentage() + "),";
        i++;
      } else {
        query += "("+i+", '" + w.getName() + "', '" + w.getType() + "', " + w.getQuantity() + ", " + w
            .getPercentage() + ");";
        j++;
      }
    }

    executeUpdate(connection, query);
  }

  public static void main(String[] argv) {
    List<Word> wordList = new ArrayList<Word>();

    readWordsFromFile(wordList);

    Connection connection = getPostGresConnection();

    createTableIfNotExist(connection);

    insertTermsInTable(connection, wordList);

    try {
      if(connection != null && !connection.isClosed()){
        connection.close();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public static boolean isNumeric(String s) {
    return s != null && s.matches("[-+]?\\d*\\.?\\d+");
  }
}
